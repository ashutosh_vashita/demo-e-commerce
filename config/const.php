<?php

return array(

    /* Date Time Format */
    'displayDateTimeFormatForAll' => "m/d/Y H:i",
    'displayDateTimeFormatWithAMPM' => "d/m/Y H:i A",
    'phpdisplayDateTimeFormatWithAMPM' => "m/d/Y h:i A",
    'displayDate' => "m/d/Y",
    'displayDateBlade' => "mm/dd/yyyy",
    'databaseStoredDateFormat' => "Y-m-d",
    'databaseStoredDateTimeFormat' => "Y-m-d H:i:s",
    'NotificationDateTimeFormat' => "Y-m-d H:i",
    'JSdisplayDateTimeFormatWithAMPM' => "MM/DD/YYYY hh:mm A",
    'dateformatFax' => "M Y",
    'fulldateformatFax' => "M D, Y, H:i A",
    'app_name' => "stocks-fantasy-league",
    'timeformat' => "H:i:s",
    "displaytime" => "h:i A",
    "timeonly" => "h:i:s",

    'statusActive' => 'active',
    'statusInActive' => 'inactive',
    'statusPending' => 'pending',
    'statusCompleted' => 'completed',

    'statusActiveInt' => 1,
    'statusInActiveInt' => 0,
    'statusPendingInt' => 2,

    'roleSuperAdmin' => 1,
    'roleAdmin' => 2,
    'roleAppUserParticipant' => 3,
    'roleFacilitator' => 4,

    'loginSuccess' => '2',
    'signupSuccess' => '1',

    'deleted' => 1,
    'nonDeleted' => 0,

    'is_login' => 1,
    'is_not_login' => 0,
);
