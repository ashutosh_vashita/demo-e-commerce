@extends('layouts.master')
@section('title', 'AdminLTE | Category Info')
@section('content')

    <!-- BEGIN: Content-->
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Add Category</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <a href="{{ route('category.create') }}">
                <button type="button" style="width: 100px" class="btn btn-block btn-outline-primary">Add</button>
            </a>
            <br>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Category Information</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if (Session::has('message'))
                                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                                    {{ Session::get('message') }}</p>
                            @endif
                            <table id="example2" class="table table-bordered table-hover">
                                <thead align="center">
                                    <tr>
                                        <th>ID</th>
                                        <th>Category Name</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody align="center">
                                    @foreach ($category as $data)
                                        <tr id="row-{{ $data->id }}">
                                            <td>{{ $data->id }}</td>
                                            <td>{{ $data->name }}</td>
                                            @if ($data->status == '0')
                                                <td>Inactive</td>
                                            @else
                                                <td>Active</td>
                                            @endif
                                            <td>{{ $data->created_at->format('d/m/Y') }}</td>
                                            <td>
                                                <a href="{{ route('category.show', $data->id) }}" data-toggle="tooltip"
                                                    data-placement="bottom" title="Show"><i class="fas fa-eye"
                                                        aria-hidden="true"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="{{ route('category.edit', $data->id) }}" data-toggle="tooltip"
                                                    data-placement="bottom" title="Update"><i class="fas fa-edit"
                                                        aria-hidden="true"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="javascript:void(0)" id="{{ $data->id }}" class="dlt"
                                                    data-toggle="tooltip" data-placement="bottom" title="Delete">
                                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- END: Content-->
@endsection

@section('pagejs')
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
            var table;

            var initTable1 = function() {
                table = $('#example2');
                table.DataTable({
                    "responsive": true,
                    "lengthChange": true,
                    "autoWidth": true,
                    "paging": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "columnDefs": [{
                        width: 100,
                        targets: 4
                    }],
                    "fixedColumns": true,
                    "pageLength": 5,
                });
            };

            $(document).on("click", ".dlt", function() {
                var id = $(this).attr("id")
                // alert(id);
                Swal.fire({
                    text: '{{ trans('admin.deleteSure') }}',
                    icon: "warning",
                    showCancelButton: !0,
                    buttonsStyling: !1,
                    confirmButtonText: "Yes, delete!",
                    cancelButtonText: "No, cancel",
                    customClass: {
                        confirmButton: "btn fw-bold btn-danger",
                        cancelButton: "btn fw-bold btn-active-dark-primary"
                    }
                }).then(function(isConfirm) {
                    if (isConfirm.value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: baseUrl + '/admin/category/' + id,
                            type: "DELETE",
                            dataType: 'json',
                            success: function(data) {
                                if (data == 'Error') {
                                    toastr.error("@lang('admin.oopserror')",
                                        "@lang('admin.error')");
                                } else {
                                    toastr.success('@lang('admin.recordDelete')',
                                        '@lang('admin.success')');
                                    // $('#example2').DataTable().reload();
                                    $('#row-' + id).remove();
                                }
                            },
                            error: function(data) {
                                toastr.error("@lang('admin.oopserror')",
                                    "@lang('admin.error')");
                            }
                        });
                    }
                })
            });
        });
    </script>
@endsection
