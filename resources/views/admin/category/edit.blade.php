@extends('layouts.master')
@section('title', 'Category Update')
@section('content')

    <!-- Horizontal Form -->
    <br>
    <section class="content">
        <div class="container-fluid">

            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Update Category Information</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('category.update', $data['id']) }}" id="category_form" method="post">
                        {{csrf_field()}}
                        {{ method_field('PUT')}}
                        <div class="card-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" id="name" name="name" value="{{$data->name}}" class="form-control"
                                    placeholder="Enter Category Name" required>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control select2bs4" name="status" id="v"
                                    data-placeholder="Select Status" style="width: 100%;">
                                    <option disabled selected value="">-- Select Status --</option>
                                    <option {{ ($data->status) == '0' ? 'selected' : '' }} value="0">Inactive</option>
                                    <option {{ ($data->status) == '1' ? 'selected' : '' }} value="1">Active</option>
                                </select>
                            </div>
                            <!-- /.card -->
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update</button>
                                <a href="{{ route('category.index') }}"><button type="button"
                                        class="btn btn-secondary cancelbutton">Cancel</button></a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.card -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#category_form").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>

@endsection
