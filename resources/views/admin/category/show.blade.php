@extends('layouts.master')
@section('title', 'Category View')
@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Category Information</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href="{{ route('category.index') }}"><button type="button"
                            style="float: right" class="btn btn-secondary cancelbutton">Back</button></a>
                        <dl class="row">
                            <dt class="col-3">Category Name</dt>
                            <dd class="col-9">
                                <p>{{ $data->name }}</p>
                            </dd>

                            <dt class="col-3">Status</dt>
                            <dd class="col-9">
                                @if ($data->status == '0')
                                    Inactive
                                @else
                                    Active
                                @endif
                            </dd>
                        </dl>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
