@extends('layouts.master')
@section('title', 'Product View')
@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Product Information</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href="{{ route('product.index') }}"><button type="button" style="float: right"
                                class="btn btn-secondary cancelbutton">Back</button></a>
                        <dl class="row">

                            <dt class="col-3">Category Name</dt>
                            <dd class="col-9">
                                <p>{{ $data->category->name }}</p>
                            </dd>

                            <dt class="col-3">Name</dt>
                            <dd class="col-9">
                                <p>{{ $data->name }}</p>
                            </dd>

                            <dt class="col-3">Description</dt>
                            <dd class="col-9">
                                <p>{{ $data->description }}</p>
                            </dd>

                            <dt class="col-3">Image</dt>
                            <dd class="col-9">
                                <p><img src="{{ asset('images/Product_images/' . $data->image) }}" width="50px"
                                        height="50px" class="img-circle"
                                        onerror="this.onerror=null;this.src='{{ asset('storage/images/default/no_image.png') }}';">
                                </p>
                            </dd>

                            <dt class="col-3">Price</dt>
                            <dd class="col-9">
                                <p>{{ $data->price }}</p>
                            </dd>

                            <dt class="col-3">Discount</dt>
                            <dd class="col-9">
                                <p>{{ $data->discount }}</p>
                            </dd>

                            <dt class="col-3">Stock</dt>
                            <dd class="col-9">
                                <p>{{ $data->stock }}</p>
                            </dd>

                            <dt class="col-3">Status</dt>
                            <dd class="col-9">
                                @if ($data->status == '0')
                                    Inactive
                                @else
                                    Active
                                @endif
                            </dd>

                        </dl>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
