@extends('layouts.master')
@section('title', 'Product Create')
@section('content')

    <!-- Horizontal Form -->
    <br>
    <section class="content">
        <div class="container-fluid">

            <div class="col-md-6">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Product Information</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('product.store') }}" id="product_form" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Product Category<span class="required">*</span></label>
                                <select class="form-control select2bs4" name="category_id" id="category_id"
                                    data-placeholder="Select Category" style="width: 100%;">
                                    <option disabled selected value="">-- Select Status --</option>
                                    @foreach ($category as $data)
                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Product Name<span class="required">*</span></label>
                                <input type="text" id="name" name="name" class="form-control"
                                    placeholder="Enter Product Name" required>
                            </div>

                            <div class="form-group">
                                <label>Product Description<span class="required">*</span></label>
                                <textarea class="form-control" id="description" name="description" placeholder="Enter Product Description " rows="3"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="image">Product Image<span class="required">*</span></label>
                                <div class="custom-file">
                                    <input type="file" class="form-control custom-file-input" id="image" name="image"
                                        accept="image/*">
                                    <label class="custom-file-label" for="image">Choose file</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Product Price<span class="required">*</span></label>
                                <input type="number" id="price" name="price" class="form-control"
                                    placeholder="Enter Product Price" required>
                            </div>

                            <div class="form-group">
                                <label>Product Discount<span class="required">*</span></label>
                                <input type="number" id="discount" name="discount" class="form-control"
                                    placeholder="Enter Product Discount" required>
                            </div>

                            <div class="form-group">
                                <label>Product Stock<span class="required">*</span></label>
                                <input type="number" id="stock" name="stock" class="form-control"
                                    placeholder="Enter Product Stock" required>
                            </div>

                            <div class="form-group">
                                <label>Status<span class="required">*</span></label>
                                <select class="form-control select2bs4" name="status" id="status"
                                    data-placeholder="Select Status" style="width: 100%;">
                                    <option disabled selected value="">-- Select Status --</option>
                                    <option value="0">Inactive</option>
                                    <option value="1">Active</option>
                                </select>
                            </div>
                            <!-- /.card -->
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Save</button>
                                <a href="{{ route('product.index') }}"><button type="button"
                                        class="btn btn-secondary cancelbutton">Cancel</button></a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- /.card -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#product_form").validate({
                rules: {
                    category_id: {
                        required: true,
                    },
                    name: {
                        required: true,
                    },
                    description: {
                        required: true,
                    },
                    image: {
                        required: true,
                        extension: "jpg|jpeg|png"
                    },
                    price: {
                        required: true,
                    },
                    discount: {
                        required: true,
                    },
                    stock: {
                        required: true,
                    },
                    status: {
                        required: true,
                    },
                },
                messages: {
                    image: {
                        required: "Please upload product image.",
                        extension: "The image must be a file of type: jpg, jpeg, png."
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>

@endsection
