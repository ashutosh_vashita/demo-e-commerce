@extends('layouts.frontmaster')
@section('title', 'Our Products')
@section('content')
    <style type="text/css">
        .my-active span {
            background-color: #5cb85c !important;
            color: white !important;
            border-color: #5cb85c !important;
        }
    </style>
    {{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
    <!-- cartstyle css -->
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/cartstyle.css') }}">
    <!-- our products -->
    <div class="products">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="titlepage">
                        <h2>Our Products</h2>
                    </div>

                    <div class="d-flex justify-content-center align-items-center w-100">
                        <div>
                            
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="form-group">
                        @include('errormessage')
                        <select name="filter_categoryname" id="filter_categoryname" class="form-control">
                            <option value="">Select Category</option>
                            @foreach ($category as $categoryname)
                                <option value="{{ $categoryname->id }}">{{ $categoryname->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group" align="center">
                        <button type="button" name="filter" id="filter" class="btn btn-info">Filter</button>
                        <button type="button" name="reset" id="reset" class="btn btn-default">Reset</button>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
            <br />

            <div id="fetch_products" class="row">
                @foreach ($product as $data)
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div id="ho_bo" class="our_products">
                            <div class="product">
                                <figure><img src="{{ asset('images/Product_images/' . $data->image) }}"
                                        style="height: 200px"
                                        onerror="this.onerror=null;this.src='{{ asset('storage/images/default/no_image.png') }}';">
                                </figure>
                            </div>
                            <h3>{{ $data->name }}</h3>
                            <span>Rs.{{ $data->price }}</span>
                            <p style="height: 60px">{{ $data->description }} </p>
                            <p class="btn-holder"><a href="{{ route('add.to.cart', $data->id) }}"
                                    class="btn btn-warning btn-block text-center" role="button">Add to cart</a> </p>
                        </div>
                    </div>
                @endforeach
                <div class="d-flex justify-content-center align-items-center w-100">
                    <div>
                        {{ $product->links('vendor.pagination.custom') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end our products -->

    <!--  contact -->
    <div class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="titlepage">
                        <h2>Contact Us</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6  padding_right0">
                    <div class="map_main">
                        <div class="map-responsive">
                            <iframe
                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=Eiffel+Tower+Paris+France"
                                width="600" height="453" frameborder="0" style="border:0; width: 100%;"
                                allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 padding_left0">
                    <form id="request" class="main_form">
                        <div class="row">
                            <div class="col-md-12 ">
                                <input class="contactus" placeholder="Name" type="type" name="Name">
                            </div>
                            <div class="col-md-12">
                                <input class="contactus" placeholder="Phone" type="type" name="Phone">
                            </div>
                            <div class="col-md-12">
                                <input class="contactus" placeholder="Email" type="type" name="Email">
                            </div>
                            <div class="col-md-12">
                                <textarea class="textarea" placeholder="Message" type="type" Message="Message">Message</textarea>
                            </div>
                            <div class="col-md-12">
                                <button class="send_btn">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end contact -->
@endsection

@section('pagejs')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#filter').click(function() {
                var categoryname = $('#filter_categoryname').val();
                $.ajax({
                    url: "{{ route('fetchrecords') }}",
                    type: 'get',
                    data: {
                        'category': categoryname,
                    },
                    success: function(data) {
                        $('#fetch_products').html("");
                        data.forEach(element => {
                            html = `<div class="col-lg-3 col-md-6 col-sm-6">
                        <div id="ho_bo" class="our_products">
                            <div class="product">
                                <figure><img src="{{ asset('images/Product_images/') }}/` + element.image + `"
                                        style="height: 200px"
                                        onerror="this.onerror=null;this.src='{{ asset('storage/images/default/no_image.png') }}';">
                                </figure>
                            </div>
                            <h3>` + element.name + `</h3>
                            <span>Rs. ` + element.price + `</span>
                            <p style="height: 60px">` + element.description + `</p>
                            <p class="btn-holder"><a href="{{ route('add.to.cart', $data->id) }}"
                                    class="btn btn-warning btn-block text-center" role="button">Add to cart</a> </p>
                        </div>
                    </div>`;
                            $('#fetch_products').append(html);
                        });
                    }
                });
            });

            $('#reset').click(function() {
                $('#filter_categoryname').val('');
                $.ajax({
                    url: "{{ route('fetchrecords') }}",
                    type: 'get',
                    success: function(data) {
                        $('#fetch_products').html("");
                        data.forEach(element => {
                            html = `<div class="col-lg-3 col-md-6 col-sm-6">
                        <div id="ho_bo" class="our_products">
                            <div class="product">
                                <figure><img src="{{ asset('images/Product_images/') }}/` + element.image + `"
                                        style="height: 200px"
                                        onerror="this.onerror=null;this.src='{{ asset('storage/images/default/no_image.png') }}';">
                                </figure>
                            </div>
                            <h3>` + element.name + `</h3>
                            <span>Rs. ` + element.price + `</span>
                            <p style="height: 60px">` + element.description + `</p>
                            <p class="btn-holder"><a href="{{ route('add.to.cart', $data->id) }}"
                                    class="btn btn-warning btn-block text-center" role="button">Add to cart</a> </p>
                        </div>
                    </div>`;
                            $('#fetch_products').append(html);
                        });
                    }
                });
            });

        });
    </script>

@endsection
