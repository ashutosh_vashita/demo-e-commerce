@extends('layouts.master')
@section('title', 'My Profile')
@section('content')

    <!-- Horizontal Form -->
    <br>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">My Profile</h3>
                        </div>
                        @include('errormessage')
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" id="updateprofile"
                            action="{{ route('updatemyprofile') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                {{-- Email --}}
                                <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input id="email" type="text" value="{{ $data->email }}" class="form-control"
                                            name="email" readonly>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                {{-- State --}}
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">State</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2bs4" name="state_id" id="state_id"
                                            data-placeholder="Select State" style="width: 100%;">
                                            <option></option>
                                            @foreach ($states as $statedata)
                                                <option {{ $data->state_id == $statedata->id ? 'selected' : '' }}
                                                    value="{{ $statedata->id }}">
                                                    {{ $statedata->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                {{-- City --}}
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">City</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2bs4" name="city_id" id="city_id"
                                            data-placeholder="Select City" style="width: 100%;">
                                            @foreach ($cities as $citydata)
                                                <option {{ $data->city_id == $citydata->id ? 'selected' : '' }}
                                                    value="{{ $citydata->id }}">
                                                    {{ $citydata->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                {{-- Image --}}
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Profile Image</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" name="image" id="image" class="custom-file-input">
                                                <label class="custom-file-label" for="image">{{ $data->image }}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Image Preview --}}
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Image Preview</label>
                                    <div class="col-sm-10">
                                        @if (isset(Auth::user()->image))
                                            <img src="{{ asset('images/User_images/' . Auth::user()->image) }}"
                                                class="img-circle" style="height: 50px; width:50px;"
                                                onerror="this.onerror=null;this.src='{{ asset('storage/images/default/no_image.png') }}';">
                                        @else
                                            <img src="{{ asset('storage/images/default/no_image.png') }}"
                                                style="height: 50px; width:50px;" class="img-circle">
                                        @endif
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                    <a href="{{ route('home') }}"><button type="button"
                                            class="btn btn-secondary cancelbutton">Cancel</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.card -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#updateprofile").validate({
                rules: {
                    state_id: {
                        required: true,
                    },
                    city_id: {
                        required: true,
                    },
                    image: {
                        extension: "jpg|jpeg|png"
                    }
                },
                messages: {
                    image: {
                        extension: "The image must be a file of type: jpg, jpeg, png."
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group row').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });

            $('#state_id').on('change', function() {
                var idState = this.value;
                $("#city_id").html('');
                $.ajax({
                    url: "{{ url('fetch-cities') }}",
                    type: "POST",
                    data: {
                        state_id: idState,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(res) {
                        $('#city_id').html('<option value="">Select City</option>');
                        $.each(res.cities, function(key, value) {
                            $("#city_id").append('<option value="' + value
                                .id + '">' + value.name + '</option>');
                        });
                    }
                });
            });
        });
    </script>

@endsection
