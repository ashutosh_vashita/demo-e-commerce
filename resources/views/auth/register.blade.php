@extends('layouts.app')
@section('title', 'AdminLTE | Registration')
@section('content')
    <div class="register-box">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="{{ route('register') }}" class="h1"><b>Admin</b>LTE</a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Register a new membership</p>
                @include('errormessage')
                <form action="{{ route('register') }}" id="usercreate" method="post" enctype="multipart/form-data">
                    @csrf
                    {{-- Username --}}
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Email --}}
                    <div class="form-group">
                        <div class="input-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Password --}}
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" class="form-control" name="password" id="password"
                                placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Confirm Password --}}
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" class="form-control" name="password_confirmation"
                                id="password_confirmation" placeholder="Retype password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Role --}}
                    <div class="form-group">
                        <select class="form-control select2bs4" name="role_id" id="role_id"
                            data-placeholder="Select User Role" style="width: 100%;">
                            <option></option>
                            <option value="1">Admin</option>
                            <option value="2">User</option>
                        </select>
                    </div>

                    {{-- State --}}
                    <div class="form-group">
                        <select class="form-control select2bs4" name="state_id" id="state_id"
                            data-placeholder="Select State" style="width: 100%;">
                            <option></option>
                            @foreach ($states as $data)
                                <option value="{{ $data->id }}">
                                    {{ $data->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    {{-- City --}}
                    <div class="form-group">
                        <select class="form-control select2bs4" name="city_id" id="city_id" data-placeholder="Select City"
                            style="width: 100%;">
                        </select>
                    </div>

                    {{-- Gender --}}
                    <div class="form-group">
                        <label>Gender : &nbsp;&nbsp;</label>
                        <div class="icheck-primary d-inline">
                            <input type="radio" id="male" name="gender" class="form-control" value="1">
                            <label for="male">
                                Male&nbsp;&nbsp;
                            </label>
                        </div>
                        <div class="icheck-primary d-inline">
                            <input type="radio" id="female" name="gender" class="form-control" value="2">
                            <label for="female">
                                Female
                            </label>
                        </div>
                    </div>

                    {{-- Hobbies --}}
                    <div class="form-group">
                        <label>Hobbie : &nbsp;&nbsp;</label>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="cricket" name="hobbie[]" value="cricket">
                            <label for="cricket">
                                Cricket&nbsp;
                            </label>
                        </div>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="travel" name="hobbie[]" value="travel">
                            <label for="travel">
                                Travel&nbsp;
                            </label>
                        </div>
                        <div class="icheck-primary d-inline">
                            <input type="checkbox" id="chess" name="hobbie[]" value="chess">
                            <label for="chess">
                                Chess
                            </label>
                        </div>
                    </div>

                    {{-- Image --}}
                    <div class="form-group">
                        <label for="image">Profile Image</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="image" id="image" class="custom-file-input">
                                <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                        </div>
                    </div>

                    {{-- Terms --}}
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                                <label for="agreeTerms">
                                    I agree to the <a href="">terms</a>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
            </div>
            <!-- /.form-box -->
        </div><!-- /.card -->
    </div>
    <!-- /.register-box -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#usercreate").validate({
                rules: {
                    username: {
                        required: true,
                        maxlength: 50
                    },
                    email: {
                        required: true,
                        maxlength: 50
                    },
                    password: {
                        required: true,
                        // minlength: 8,
                        // maxlength: 20,
                        password: true,
                    },
                    password_confirmation: {
                        required: true,
                        // minlength: 8,
                        // maxlength: 20,
                        equalTo: "#password",
                        password: true,
                    },
                    role_id: {
                        required: true,
                    },
                    state_id: {
                        required: true,
                    },
                    city_id: {
                        required: true,
                    },
                    gender: {
                        required: true,
                    },
                    'hobbie[]': {
                        required: true,
                    },
                    image: {
                        required: true,
                        extension: "jpg|jpeg|png"
                    }
                },
                messages: {
                    image: {
                        extension: "The image must be a file of type: jpg, jpeg, png."
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });

            $('#state_id').on('change', function() {
                var idState = this.value;
                $("#city_id").html('');
                $.ajax({
                    url: "{{ url('fetch-cities') }}",
                    type: "POST",
                    data: {
                        state_id: idState,
                        _token: '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success: function(res) {
                        $('#city_id').html('<option value="">Select City</option>');
                        $.each(res.cities, function(key, value) {
                            $("#city_id").append('<option value="' + value
                                .id + '">' + value.name + '</option>');
                        });
                    }
                });
            });
        });
    </script>

@endsection
