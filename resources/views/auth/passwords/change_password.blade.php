@extends('layouts.master')
@section('title', 'Change Password')
@section('content')

    <!-- Horizontal Form -->
    <br>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Change Password</h3>
                        </div>
                        @include('errormessage')
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" id="changepassword"
                            action="{{ route('changePasswordPost') }}">
                            @csrf
                            <div class="card-body">
                                {{-- Current Password --}}
                                <div class="form-group">
                                    <label for="current_password" class="col-sm-4 col-form-label">Current Password</label>
                                    <div class="col-sm-10">
                                        <input id="current_password" name="current_password" type="password"
                                            class="form-control" placeholder="Enter Current Password">
                                    </div>
                                </div>

                                {{-- New Password --}}
                                <div class="form-group">
                                    <label for="new_password" class="col-sm-2 col-form-label">New Password</label>
                                    <div class="col-sm-10">
                                        <input id="new_password" name="new_password" type="password" class="form-control"
                                            placeholder="Enter New Password">
                                    </div>
                                </div>

                                {{-- Confirm Password --}}
                                <div class="form-group">
                                    <label for="new_password_confirmation" class="col-sm-4 col-form-label">Confirm New
                                        Password</label>
                                    <div class="col-sm-10">
                                        <input id="new_password_confirmation" name="new_password_confirmation"
                                            type="password" class="form-control" placeholder="Enter Confirm Password">
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Change Password</button>
                                    <a href="{{ route('home') }}"><button type="button"
                                            class="btn btn-secondary cancelbutton">Cancel</button></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.card -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#changepassword").validate({
                rules: {
                    current_password: {
                        required: true,
                    },
                    new_password: {
                        required: true,
                        password: true,
                    },
                    new_password_confirmation: {
                        required: true,
                        equalTo: "#new_password",
                        password: true,
                    }
                },
                errorElement: 'span',
                errorPlacement: function(error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>

@endsection
