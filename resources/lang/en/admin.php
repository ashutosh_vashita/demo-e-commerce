<?php

return [

    /* Admin Section Start */
    'siteName' => 'Story Formed Life',
    'superAdmin' => 'Super Admin',
    'footerEmailTeam' => 'Story Formed Life Team',
    'footer' => 'Story Formed Life | All rights reserved',
    'success' => "Success !!",
    'error' => "Error !!",
    'update' => "Updated !!",
    "oopserror" => "Oops, something went wrong..",
    "somethingWrong" => "Oops, There is some thing went wrong.Please try after some time.",
    "recordDelete" => "Record has been deleted successfully.",
    "recordUpdate" => "Record has been updated successfully.",
    "strongPassword" => "Your Password must contains one Uppercase, Lowercase, Digit and Special Character",
    "no_admin_aceess" => "No Admin Access",
    "inactiveAccount" => "Sorry, Your account is inactive, contact to administrator.",

    "deleteSure" => "Are you sure you want to delete this record?",
    "deleteSureImage" => "Are you sure you want to delete this image?",

    /* Login */
    "emailNotVerified" => "Your account is not verified, Please check your email to verify your account",
    "accountInactive" => "Your account is inactive, Please contact administrator",
    "notAuthorized" => "You are not authorized user to access this account",
    "invalidCredentials" => "Invalid Credentials",
    "emailverified" => "Thank you, your email has been confirmed. Please click  to login to the app!",
    'emailAlreadyVerifyed' => "Your email address has already been verified.",

    /* Logout */
    'logoutSure' => "Are you sure you want to logout?",

    /* Change Password */
    "currentPasdswordNotmatch" => "Your current password does not matches with password you provided. Please try again.",
    "passwordChanged" => "Your password has been changed successfully.",

    /* My profile */
    'emailExists' => "Email is already exists",
    "emailUpdated" => 'Email has been updated successfully',
    "updatemyProfile" => 'Profile has been updated successfully',
    "createmyProfile" => "Profile has been created successfully.",
    "removeProfile" => "Profile has been removed successfully.",

    /* Forgot Password & Reset Password */
    "notfoundEmail" => "We do not have an active account with given Email address",
    'forgotPassword' => "We have sent a link to your registered email to reset your password.",
    "InvalidResetPassword" => "Your reset password link may be expired OR Invalid token, Please try again using forgot password",
    "passwordResetSuccess" => "Your Password has been reset successfully.",
    "verificationEmailSend" => "Verification email has been send successfully.",
    "setNewPassword" => "Password has been set successfully",
    "OldPasswordReuse" => "This password has been already  used. Please choose a new password.",
    "PasswordandConfirmPassword" => "Password and Confirm Password does not match.",

    /** CMS  */
    "cmscreate" => "CMS has been created successfully.",
    "cmsupdate" => "CMS has been updated successfully.",

    /** CMS  */
    "faqcreate" => "FAQ has been created successfully.",
    "faqupdate" => "FAQ has been updated successfully.",

    /** Groups  */
    "groupcreate" => "Group has been created successfully.",
    "groupupdate" => "Group has been updated successfully.",
    "activegroupSure" => "Are you sure you want to active this group?",
    "groupActivated" => "Group has been activated successfully.",

    /* Course */
    "coursecreate" => "Course has been created successfully.",
    "courseupdate" => "Course has been updated successfully.",

    /* Course Passage */
    "coursepassagecreate" => "Course Passage has been created successfully.",
    "coursepassageupdate" => "Course Passage has been updated successfully.",

    /* Course Meditation */
    "coursemeditationcreate" => "Course Meiditation has been created successfully.",
    "coursemeditationupdate" => "Course Meiditation has been updated successfully.",

    'userscreatesuccess' => "User has been created successfully",
    'usersupdatesuccess' => "User has been updated successfully",
    'userDeleteSuccess' => "User deleted successfully",
    'userCanNotDelete' => "Group is active with this user, User can not be deleted",

    /* Resources */
    "resourcecreate" => "Resource has been created successfully.",
    "resourceupdate" => "Resource has been updated successfully.",

    "noParticipantant" => "No any participants available.",

    "notificationSure" => "Are you sure you want to send Notification ?",
    "notificationSendsuccess" => "Notification send Successfully.",

    /* Password Validation Erorr Message */
    "PasswordContainuppercase" => "Password must contain at least one uppercase.",
    "PasswordContainlowercase" => "Password must contain at least one lowercase.",
    "PasswordContainDigit" => "Password must contain at least one digit.",
    "PasswordContainSpecialcharacter" => "Password must contain special characters from @#$%&.",
    "PasswordLength" => "Please enter 8 character passwrod.",

    /* Facilitator Training  */
    "FacilitatorTrainingcreate" => "Facilitator Training has been created successfully.",
    "FacilitatorTrainingupdate" => "Facilitator Training has been updated successfully.",

    /*Training Group Ideas */

    "TrainingGroupIdeacreate" => "Training Group Idea has been created successfully.",
    "TrainingGroupIdeaupdate" => "Training Group Idea has been updated successfully.",

    /* Facilitator Handbook Beliefs */

    "facilitatorhandbookbeliefscreate" => "Facilitator hand book beliefs has been created",
    "facilitatorhandbookbeliefsupdate" => "Facilitator hand book beliefs has been updated",

    "facilitatorapprovesuccess" => "Facilitator approved successfully.",
    "approveSure" => "Are you sure you want to approve this facilitator?",

    /* Facilitator Invitation  */
    "FacilitatorInvitationSuccess" => "Facilitator Invitation has been sent successfully.",
    "FacilitatorInvited" => "This Facilitator has been already Invited.",
    "FacilitatorInvitationPending" => "This Facilitator has been already Invited.",
    "FacilitatorInvitationApproved" => "This user is aleady assigned as facilitator.",
    "GroupNotActivated" => "You can not invite facilitator because this group is not activated.",
    'FacilitatorAlreadyAssigned' => "You can not send invitaion, Facilitator already assigned to this group.",
];
