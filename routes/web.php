<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DropdownController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\UsersController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\admin\ProductController;
use App\Http\Controllers\admin\CategoryController;
use App\Http\Controllers\Auth\ChangePasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// Google login
Route::get('login/google', [LoginController::class, 'redirectToGoogle'])->name('login.google');
Route::get('login/google/callback', [LoginController::class, 'handleGoogleCallback']);

// Github login
Route::get('login/github', [LoginController::class, 'redirectToGithub'])->name('login.github');
Route::get('login/github/callback', [LoginController::class, 'handleGithubCallback']);

/* User Dashboard */
Route::get('/home', [HomeController::class, 'index'])->name('home');

/* Admin Dashboard */
Route::get('admin/home', [HomeController::class, 'adminHome'])->name('adminhome');

/* Fetch City From States Using AJAX */
Route::post('fetch-cities', [DropdownController::class, 'fetchCity']);

/* LogOut */
Route::get('/logout', [LogoutController::class, 'logout'])->name('logout');

/* My profile */
Route::get('myprofile', [UsersController::class, 'myProfile'])->name('myprofile');
Route::post('updatemyprofile', [UsersController::class, 'updateMyProfile'])->name('updatemyprofile');

/* Change Password */
Route::get('/changePassword', [ChangePasswordController::class, 'showChangePasswordGet'])->name('changePasswordGet');
Route::post('/changePassword', [ChangePasswordController::class, 'changePasswordPost'])->name('changePasswordPost');

/* Fetch Records Using AJAX */
Route::get('/fetchrecord', [HomeController::class, 'fetchrecords'])->name('fetchrecords');

/* Add To Cart */
Route::get('add-to-cart/{id}', [HomeController::class, 'addToCart'])->name('add.to.cart');

/* View Cart */
Route::get('cart', [HomeController::class, 'cart'])->name('cart');

/* Update Cart */
Route::patch('update-cart', [HomeController::class, 'update'])->name('update.cart');

/* Delete Cart */
Route::delete('remove-from-cart', [HomeController::class, 'remove'])->name('remove.from.cart');

Route::middleware(['is_admin'])->group(function () {
    Route::group(['prefix' => 'admin'], function () {

        /* Category Section */
        Route::resource('category', CategoryController::class);

        /* Product Section */
        Route::resource('product', ProductController::class);
    });
});
