<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            [
                'id' => 1,
                'name' => 'Goa',

            ],

            [
                'id' => 2,
                'name' => 'Gujarat',

            ],

            [
                'id' => 3,
                'name' => 'Maharashtra',

            ],
            [
                'id' => 4,
                'name' => 'Punjab',

            ],
            [
                'id' => 5,
                'name' => 'Rajasthan',

            ],
        ];

        State::insert($states);
    }
}
