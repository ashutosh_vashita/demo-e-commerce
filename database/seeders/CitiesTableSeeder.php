<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'id' => 1,
                'state_id' => 1,
                'name' => 'Vasco da Gama',
            ],
            [
                'id' => 2,
                'state_id' => 1,
                'name' => 'Margao',
            ],
            [
                'id' => 3,
                'state_id' => 1,
                'name' => 'Panaji',
            ],
            [
                'id' => 4,
                'state_id' => 1,
                'name' => 'Mapusa',
            ],
            [
                'id' => 5,
                'state_id' => 1,
                'name' => 'Ponda',
            ],
            [
                'id' => 6,
                'state_id' => 2,
                'name' => 'Ahmedabad',
            ],
            [
                'id' => 7,
                'state_id' => 2,
                'name' => 'Surat',
            ],
            [
                'id' => 8,
                'state_id' => 2,
                'name' => 'Baroda',
            ],
            [
                'id' => 9,
                'state_id' => 2,
                'name' => 'Rajkot',
            ],
            [
                'id' => 10,
                'state_id' => 2,
                'name' => 'Gandhinagar',
            ],
            [
                'id' => 11,
                'state_id' => 3,
                'name' => 'Mumbai',
            ],
            [
                'id' => 12,
                'state_id' => 3,
                'name' => 'Nashik',
            ],
            [
                'id' => 13,
                'state_id' => 3,
                'name' => 'Pune',
            ],
            [
                'id' => 14,
                'state_id' => 3,
                'name' => 'Nagpur',
            ],
            [
                'id' => 15,
                'state_id' => 3,
                'name' => 'Thane',
            ],
            [
                'id' => 16,
                'state_id' => 4,
                'name' => 'Amritsar',
            ],
            [
                'id' => 17,
                'state_id' => 4,
                'name' => 'Patiala',
            ],
            [
                'id' => 18,
                'state_id' => 4,
                'name' => 'Ludhiana',
            ],
            [
                'id' => 19,
                'state_id' => 4,
                'name' => 'Jalandhar',
            ],
            [
                'id' => 20,
                'state_id' => 4,
                'name' => 'Mohali',
            ],
            [
                'id' => 21,
                'state_id' => 5,
                'name' => 'Jaipur',
            ],
            [
                'id' => 22,
                'state_id' => 5,
                'name' => 'Jaisalmer',
            ],
            [
                'id' => 23,
                'state_id' => 5,
                'name' => 'Udaipur',
            ],
            [
                'id' => 24,
                'state_id' => 5,
                'name' => 'Jodhpur',
            ],
            [
                'id' => 25,
                'state_id' => 5,
                'name' => 'Bikaner',
            ],
        ];

        City::insert($cities);
    }
}
