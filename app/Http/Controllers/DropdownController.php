<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;

class DropdownController extends Controller
{
    public function index()
    {
        $data['states'] = State::get(["name", "id"]);
        return view('auth.register', $data);
    }

    public function fetchCity(Request $request)
    {
        $data['cities'] = City::where("state_id", $request->input('state_id'))->get(["name", "id"]);
        return response()->json($data);
    }
}
