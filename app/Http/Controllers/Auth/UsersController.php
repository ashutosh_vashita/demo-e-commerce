<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /* My profile Start*/
    public function myProfile()
    {
        $data = User::getUserDetails(Auth::user()->id);
        $states = State::getStateDetails();
        $cities = City::getCityDetails();
        return view('auth.myprofile', compact('data', 'states', 'cities'));

    }

    public function updateMyProfile(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'state_id' => 'required',
                'city_id' => 'required',
                'image' => 'image|mimes:jpg,png',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator->errors());
            }

            User::updateMyProfile($request);

            session()->flash('success', "Profile Updated Successfully!!");
            return redirect()->route('myprofile');
        } catch (\Exception $e) {
            session()->flash('error', $e->getMessage());
            return back()->withInput();
        }
    }
}
