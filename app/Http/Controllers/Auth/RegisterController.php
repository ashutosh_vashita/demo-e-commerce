<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'username' => 'required|unique:users,username|max:50',
            'email' => 'required|email|unique:users,email|max:50',
            'password' => 'required',
            'password_confirmation' => 'same:password',
            'role_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'gender' => 'required',
            'hobbie' => 'required',
            'image' => 'required',
        ]);

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        // return User::create([
        //     'name' => $data['name'],
        //     'email' => $data['email'],
        //     'password' => Hash::make($data['password']),
        // ]);

        $data = new User();
        $data->username = request()->username;
        $data->email = request()->email;
        if (isset(request()->password) && request()->password != '') {
            $data->password = Hash::make(request()->password);
        }
        $data->email_verified_at = Carbon::now();

        if (isset(request()->image) && request()->image != '') {
            /* Unlink Image */
            if (isset($data->image) && $data->image != '') {
                $imagePath = 'images/blog_images/' . '' . $data->image;
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
            $image = request()->image;
            $imageName = 'User-' . time() . '.' . request()->image->getClientOriginalExtension();
            $image->move('images/User_images/', $imageName);
            $data->image = $imageName;
        }
        $data->role_id = request()->role_id;
        $data->state_id = request()->state_id;
        $data->city_id = request()->city_id;
        $data->gender = request()->gender;
        $data->hobbie = implode(',', request()->hobbie);

        $data->save();

        // session()->flash('success', "User created successfully!!");
        request()->session()->flash('success', 'User created successfully!!');
        return redirect()->route('login');
    }

    public function showRegistrationForm()
    {
        $data['states'] = State::get(["name", "id"]);
        // dd($data);
        return view('auth.register', $data);
    }

}
