<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CustomSearchController extends Controller
{
    function index(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->filter_gender)) {
                $data = Category::all()
                    ->where('name', $request->filter_categoryname);
            } else {
                $data = Category::all();
            }
            return view('userdashboard', compact('data'));
        }
        $category = DB::table('categories')
            ->select('name')
            ->groupBy('name')
            ->orderBy('id', 'ASC')
            ->get();
        return view('userdashboard', compact('category'));
    }
}
