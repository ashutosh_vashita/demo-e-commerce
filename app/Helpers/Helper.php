<?php

namespace App\Helpers;

use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class Helper
{
    public static function Status($data)
    {
        if ($data->status == config('const.statusActiveInt')) {
            return '<span class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill">Active</span>';
        } else if ($data->status == config('const.statusInActiveInt')) {
            return '<span class="kt-badge  kt-badge--yellow kt-badge--inline kt-badge--pill " style="color: #ffffff;
                    background: #ffb822 !important;">InActive</span>';
        }{
            return '<button type="button" class="btn red btn-xs pointerhide cursornone">---</button>';
        }
    }

    public static function Action($editLink = '', $deleteID = '', $viewLink = '', $recoverylink = '', $emailLink = '', $progressLink = '', $email = '', $pushNotification = '')
    {
        if ($editLink)
        //$edit = '<a href="' . $editLink . '"  data-toggle="tooltip" title="Edit"> <i class="la la-edit"></i></a>';
        {
            $edit = '<a href="' . $editLink . '"  data-toggle="tooltip" title="Edit" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View"><i class="flaticon-edit"></i>
                        </a>';
        } else {
            $edit = '';
        }

        if ($deleteID) {
            $delete = '<a onclick="deleteValueSet(' . $deleteID . ')"  class="btn btn-sm btn-clean btn-icon btn-icon-md"  title="Delete" data-toggle="modal" data-target="#delete-modal" >  <i class="flaticon-delete"></i></a>';
        } else {
            $delete = '';
        }

        if ($viewLink) {
            $view = '<a  href="' . $viewLink . '" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="flaticon-eye"></i></a>';
        } else {
            $view = '';
        }

        return $view . '' . $edit . '' . $delete . '';
    }

    public static function getTimezone()
    {
        if (Session::get('customTimeZone') && Session::get('customTimeZone') != '') {
            return Session::get('customTimeZone');
        } else {
            return "Europe/Berlin";
        }

    }

    public static function displayDateTimeConvertedWithFormat($date, $format = '')
    {
        if (!$format) {
            $format = config('const.displayDateTimeFormatForAll');
        }

        $dt = new DateTime($date);
        $tz = new DateTimeZone(Helper::getTimezone()); // or whatever zone you're after

        $dt->setTimezone($tz);
        return $dt->format($format);
    }

    public static function getSearchStatusArray()
    {
        return array(
            '1' => "Active",
            '0' => "InActive",
        );
    }

    public static function DefultdisplayProfilePath()
    {
        return URL::to('/') . '/assets/admin/user/default.jpg';
    }

    public static function displayNoImagePath()
    {
        return URL::to('/') . '/assets/images/';
    }

    /* Store Path */
    public static function profileFileUploadPath()
    {
        return storage_path('app/public/profilepic/');
    }

    /* Display Path */
    public static function displayProfilePath()
    {
        return URL::to('/') . '/storage/profilepic/';
    }

    /* User Status */
    public static function Userstatus()
    {
        return array(
            '1' => "Active",
            '0' => "InActive",
        );
    }

    public static function Categorystatus()
    {
        return array(
            '1' => "Active",
            '0' => "InActive",
        );
    }

}
