<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'status',
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public static function addEditProduct($request, $id = '')
    {
        if ($id != '') {
            $data = Product::find($id);            
        } else {
            $data = new Product();
        }
        $data->category_id = $request->category_id;
        $data->name = $request->name;
        $data->description = $request->description;

        if (isset($request->image) && $request->image != '') {
            /* Unlink Image */
            if (isset($data->image) && $data->image != '') {
                $imagePath = 'images/Product_images/' . '' . $data->image;
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
            $image = $request->image;
            $imageName = 'Product-' . time() . '.' . $request->image->getClientOriginalExtension();
            $image->move('images/Product_images/', $imageName);
            $data->image = $imageName;
        }

        $data->price = $request->price;
        $data->discount = $request->discount;
        $data->stock = $request->stock;
        $data->status = isset($request->status) ? $request->status : config('const.statusActiveInt');

        $data->save();
        return $data;
    }

    public static function getProductDetails($id)
    {
        $data = Product::find($id);
        return $data;
    }
}
