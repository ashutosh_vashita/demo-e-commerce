<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'status',
    ];

    public function product()
    {
        return $this->hasMany(Product::class, 'category_id', 'id')->where('status',config('const.statusActiveInt'));
    }

    public static function addEditCategory($request, $id = '')
    {

        if ($id != '') {
            $data = Category::find($id);
        } else {
            $data = new Category();
        }
        $data->name = $request->name;
        $data->status = isset($request->status) ? $request->status : config('const.statusActiveInt');

        $data->save();
        return $data;
    }

    public static function getCategoryDetails($id)
    {
        $data = Category::find($id);
        return $data;
    }
}
