<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'role_id',
        'state_id',
        'city_id',
        'gender',
        'hobbie',
        'image',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getUserDetails($id)
    {
        $data = User::find($id);
        return $data;
    }

    public static function updateMyProfile($request)
    {
        $data = User::find(Auth::user()->id);
        $data->state_id = isset($request->state_id) ? $request->state_id : null;
        $data->city_id = isset($request->city_id) ? $request->city_id : null;
        if (isset($request->image) && $request->image != '') {
            /* Unlink Image */
            if (isset($data->image) && $data->image != '') {
                $imagePath = 'images/User_images/' . '' . $data->image;
                if (file_exists($imagePath)) {
                    unlink($imagePath);
                }
            }
            $image = $request->image;
            $imageName = 'User-' . time() . '.' . $request->image->getClientOriginalExtension();
            $image->move('images/User_images/', $imageName);
            $data->image = $imageName;
        }
        $data->save();
        return self::getUserDetails($data->id);
    }
}
